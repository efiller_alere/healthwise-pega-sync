<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:hw="http://www.healthwise.org/2009/DocumentInfo"
                xmlns:xhtml="http://www.w3.org/1999/xhtml"
                exclude-result-prefixes="xhtml">

  <xsl:output 
  		method='html'
        omit-xml-declaration='yes'
        media-type='text/html'
        indent="no" encoding="UTF-8" />

  
  <!-- Note: To generate output without the XHTML namespace, replace the two
    <xsl:copy> tags in this file with <xsl:element name="{local-name()}">
  -->
 
  
  <!-- TODO: Modify to provide relative path from document pages to resource pages
      (location of /inc and /media folders) -->
  <xsl:param name="lang" select="''" />
  <xsl:param name="resourceRoot" select="'${IMG_SERVER_URI}alere/healthwise/99'" />


  <!-- 
      This template defines the page layout. It pulls HTML segments in from the
      prebuilt section in the source document to construct the page. 
   -->
  <xsl:template match="hw.doc|hw.list|Categories|Category|hw.static">

	   <xsl:apply-templates select="doc.prebuilt/prebuilt.content/*" />
   
  </xsl:template>



  <xsl:template match="*">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates />
    </xsl:copy>
  </xsl:template>

  
  <!-- ========================================================================= -->
  <!-- Further client customizations go below this line -->

  <xsl:template match="xhtml:div[@id='HwCustomContentBottom'] | xhtml:div[@class='HwContentHeader']" />

  <xsl:template match="xhtml:p"> 
        <xsl:element name="p" namespace="">
        	<xsl:value-of select="."></xsl:value-of>
		</xsl:element>
  </xsl:template>



  <!-- Remove Custom divs in content element-->
  <xsl:template match="xhtml:div[@id='HwCustomContentTop']">
    <xsl:choose>
      <xsl:when test="/hw.doc/@hwid-content='nutri'">
        <xsl:copy>
          <xsl:copy-of select="@*" />
          <xsl:element name="div">
            <xsl:attribute name="id">
              <xsl:value-of select="'clamMessage'"/>
            </xsl:attribute>
            <xsl:attribute name="class">
              <xsl:value-of select="concat(@class, ' HwClamMessage')"/>
            </xsl:attribute>
            <xsl:value-of select="'Need a plan to eat better?'"/>
            <xsl:element name="a">
	            <xsl:attribute name="title">
    	          <xsl:value-of select="'Health Form'"/>
        	    </xsl:attribute>
	            <xsl:attribute name="target">
    	          <xsl:value-of select="'_blank'"/>
        	    </xsl:attribute>
	            <xsl:attribute name="href">
	            <xsl:value-of select="concat('${IMG_SERVER_URI}','alere/healthwise/99/media/pdf/hw/form_aa129142.pdf')" />
        	    </xsl:attribute>
	            <xsl:value-of select="'Try this easy-to-use menu planner.'"/>
            </xsl:element>
          </xsl:element>
          <xsl:apply-templates />
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:attribute name="style">
            <xsl:value-of select="'display: none'"/>
          </xsl:attribute>
          <xsl:copy-of select="@*" />
          <xsl:element name="div">
            <xsl:attribute name="id">
              <xsl:value-of select="'clamMessage'"/>
            </xsl:attribute>
          </xsl:element>
          <xsl:apply-templates />
        </xsl:copy>

      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
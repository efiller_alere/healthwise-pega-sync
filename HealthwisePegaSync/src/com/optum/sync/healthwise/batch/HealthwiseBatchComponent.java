package com.optum.sync.healthwise.batch;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.optum.sync.healthwise.category.ExcelCategoryMapper;
import com.optum.sync.healthwise.convert.HealthwiseDom4jDocumentXMLProcessor;
import com.optum.sync.healthwise.convert.HealthwiseSolrService;
import com.optum.sync.healthwise.convert.HealthwiseSolrServiceImpl;
import com.optum.sync.healthwise.model.HealthwiseDocument;

public class HealthwiseBatchComponent  {
	
	private static String KEY = "A3MOAY42MBULTRMZRKDIEHCTENQKRDBMZIVRJGSHWNGYHBTEYFMQPWVYVQBMQIWQOJQMOVM3EJX2YLKTVMI4AKQG3IEE46PBAVCG6N34DWXA3MX4M2USSWXCURTN5G5FHEQE367PIL2G66FZ5QPZ42CH6WE";

	private static String INVENTORY = "https://ixbapi.healthwise.net/healthwise/inventory";

	private static String CONTENT = "https://ixbapi.healthwise.net/KnowledgeContent/";	 

	private static String RESOURCE = "https://ixbapi.healthwise.net/resource";
	
	private static String FORMAT_XML = "hw.format=hwxml";
	private static String FORMAT_HTML = "hw.format=rhtml";


	protected HealthwiseSolrService healthwiseSolrService;
	protected HealthwiseDom4jDocumentXMLProcessor processor;
	private String locale;
	private String healthwiseKey;
	
	
	
	public HealthwiseBatchComponent() {
		super();
		healthwiseSolrService = new HealthwiseSolrServiceImpl();
		processor = new HealthwiseDom4jDocumentXMLProcessor();
		try {
			processor.init();
		}
		catch(Exception ex) {
			System.out.println("Error init of XML processor" + ex.getMessage());
			throw new IllegalStateException("Error init of XML processor", ex);
		}

	}

	public void setSolrHost(String hostURL) {
		if (healthwiseSolrService != null) {
			healthwiseSolrService.setSolrHost(hostURL);
		}
		
	}
	public void setCategoryURL(String hostURL) {
		if (healthwiseSolrService != null) {
			ExcelCategoryMapper mapper = new ExcelCategoryMapper();
			mapper.setCategoryMappingURL(hostURL);
			healthwiseSolrService.setMapper(mapper);
		}
		
	}


	public void push() {
		
        try
        {
       		pushImpl();
                	
        }
        catch (Exception ex)
        {
        	ex.printStackTrace();
        }
 	}
	
	protected void pushImpl()
	{
		
		if (healthwiseKey == null) {
			throw new IllegalStateException("Key not set");
		}
		
       			
        	        try {
        	        	
        	        	HttpClient client = new HttpClient();
        	        	StringBuilder urlString = new StringBuilder(INVENTORY);
        	        	urlString.append("?hw.key=").append(healthwiseKey);

        	        	HttpMethod method = new GetMethod(urlString.toString());
        	        	int statusCode = client.executeMethod(method);

        	        	String responseString = IOUtils.toString(method.getResponseBodyAsStream(), "UTF-8");

        	        	method.releaseConnection();
        	        	List<HealthwiseDocument> techlist = new ArrayList<HealthwiseDocument>();
        	            processTechdocs( responseString, techlist);
        	            int increment = 40;
        	        	int begin = 0;
        	        	int end = 40;

        	        	while(end < techlist.size()) {
        	        		sendPartialListToSolr(begin, end, techlist);
        	        		begin += increment;
        	        		end += increment;
        	        		
        	        	}
        	            
        	            
        	        }
        	        catch (Exception ex) {
        	        	ex.printStackTrace();
        	        }
		
	}


    private String retreiveHealthwise(String hwid, boolean html, String locale) {

        try {
        	
        	StringBuilder urlString = new StringBuilder(CONTENT);
        	urlString.append('/').append(hwid);
        	urlString.append("?hw.key=").append(healthwiseKey);
        	if (html) {
            	urlString.append('&').append(FORMAT_HTML);
        	}
        	else {
            	urlString.append('&').append(FORMAT_XML);
        		
        	}

        	if ("es_US".equals(locale) ) {
            	urlString.append('&').append("lang=es-us");
        	}

        	HttpClient client = new HttpClient();

        	HttpMethod method = new GetMethod(urlString.toString());
        	int statusCode = client.executeMethod(method);

        	byte[] response = method.getResponseBody();
        	String responseString = new String(response);
        	return responseString;
        }
        catch (Exception ex) {
        	
        }

    	return "";
    }

    private void sendPartialListToSolr(int begin, int end, List<HealthwiseDocument> techlist ) throws DocumentException {
    	List<HealthwiseDocument> processedlist = new ArrayList<HealthwiseDocument>();

    	for (int index = begin; index < end; ++index ) {

        	HealthwiseDocument hw = techlist.get(index);
            String hwXML = retreiveHealthwise(hw.getHwid(),false, locale);
            ByteArrayInputStream documentStream = new ByteArrayInputStream(hwXML.getBytes());
            if (documentStream != null && hwXML != null && !hwXML.isEmpty()) {

            	try {
                    processor.processDocumentMetadata(hw, documentStream);
                    ByteArrayInputStream contentStream = new ByteArrayInputStream(hwXML.getBytes());

                    processor.processDocumentContent(hw, contentStream);
                    // Skip this one if loading Spanish Healthwise and article is in English.
                	if ("es_US".equals(locale) && Locale.US.equals(hw.getLocale() )) {
                		continue;
                	}

                    processedlist.add(hw);
            	}
            	catch (Exception ex) {
            		ex.printStackTrace();
            	}
            }
        }
        
        
        
    	healthwiseSolrService.processAdds(processedlist );

    	
    }
    
	/**
	 * Add documents from the given InputStream to the list, expecting the
	 * stream to contain a manifest of all of the documents contained in the
	 * archive.
	 * 
	 * @param documents
	 * @param techdocs
	 * @throws DocumentException
	 */
	@SuppressWarnings("unchecked")
	private void processTechdocs(String xmlString, List<HealthwiseDocument> documents) 
			throws IOException, DocumentException {
		
		SAXReader reader = new SAXReader();
		
		Document doc = reader.read(new StringReader(xmlString));

        Element rootNode = doc.getRootElement();

        List<Element> statuses = rootNode.elements("content.status");

		for (Element status : statuses) {
			HealthwiseDocument document = new HealthwiseDocument();
			document.setFilename(status.attributeValue("filename"));
			document.setHwid(status.attributeValue("hwid-content"));
			document.setType(status.attributeValue("type"));
			document.setStatus(status.attributeValue("status"));
			Element title = status.element("title");
			if (title != null) {
				document.setTitle(title.getText());
			}

			
			String docStatus = document.getStatus();
			documents.add(document);
		}
	}

	

	public HealthwiseSolrService getHealthwiseSolrService() {
		return healthwiseSolrService;
	}

	public void setHealthwiseSolrService(HealthwiseSolrService healthwisesolrService) {
		this.healthwiseSolrService = healthwisesolrService;
	}

	public HealthwiseDom4jDocumentXMLProcessor getProcessor() {
		return processor;
	}

	public void setProcessor(HealthwiseDom4jDocumentXMLProcessor processor) {
		this.processor = processor;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}





	public String getKey() {
		return healthwiseKey;
	}





	public void setKey(String key) {
		this.healthwiseKey = key;
	}

}

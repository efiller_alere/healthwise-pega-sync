/**
 * 
 */
package com.optum.sync.healthwise.test;

import com.optum.sync.healthwise.batch.HealthwiseBatchComponent;

/**
 * @author EFiller
 *
 */
public class RunBatch {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String KEY = "A3MOAY42MBULTRMZRKDIEHCTENQKRDBMZIVRJGSHWNGYHBTEYFMQPWVYVQBMQIWQOJQMOVM3EJX2YLKTVMI4AKQG3IEE46PBAVCG6N34DWXA3MX4M2USSWXCURTN5G5FHEQE367PIL2G66FZ5QPZ42CH6WE";		HealthwiseBatchComponent batch = new HealthwiseBatchComponent();
		batch.setLocale("en_US");
		batch.setKey(KEY);
//		batch.setSolrHost("http://solr.dev2.alere.com:8090/solr/nextgencloud");
		batch.setSolrHost("http://localhost:8082/solr/nextgencloud");

		batch.setCategoryURL("http://img.dev.alere.com/imageserver/alere/healthwise/Vendor_Category_Mapping_Rules.xls");

		batch.push();

	}

}

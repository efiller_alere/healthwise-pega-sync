package com.optum.sync.healthwise.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class HealthwiseDocument extends HealthwiseContent {

	private String type;
	private String filename;
	private String title;
	private String status;
	private String content;
	private Boolean writtenByHealthwise;
	private String documentAbstract;
	private String author;
	private String editor;
	private String associateEditor;
	private Date releaseDate;
	private String rank;
	private String navContent;
	private Set<String> keywords = new TreeSet<String>();
	private Set<String> categories = new TreeSet<String>();
	private Set<String> citations = new HashSet<String>();
	private Set<HealthwiseRelatedContent> relatedTopics = new TreeSet<HealthwiseRelatedContent>();
	private Set<HealthwiseRelatedContent> relatedContent = new TreeSet<HealthwiseRelatedContent>();
	private Set<String> worksConsulted = new HashSet<String>();
	private String subtype;

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Boolean getWrittenByHealthwise() {
		return writtenByHealthwise;
	}

	public void setWrittenByHealthwise(Boolean writtenByHealthwise) {
		this.writtenByHealthwise = writtenByHealthwise;
	}

	public String getDocumentAbstract() {
		return documentAbstract;
	}

	public void setDocumentAbstract(String documentAbstract) {
		this.documentAbstract = documentAbstract;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getAssociateEditor() {
		return associateEditor;
	}

	public void setAssociateEditor(String associateEditor) {
		this.associateEditor = associateEditor;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getNavContent() {
		return navContent;
	}

	public void setNavContent(String navContent) {
		this.navContent = navContent;
	}

	public Set<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(Set<String> keywords) {
		this.keywords = keywords;
	}

	public Set<String> getCategories() {
		return categories;
	}

	public void setCategories(Set<String> categories) {
		this.categories = categories;
	}

	public Set<String> getCitations() {
		return citations;
	}

	public void setCitations(Set<String> citations) {
		this.citations = citations;
	}

	public Set<HealthwiseRelatedContent> getRelatedTopics() {
		return relatedTopics;
	}

	public void setRelatedTopics(Set<HealthwiseRelatedContent> relatedTopics) {
		this.relatedTopics = relatedTopics;
	}

	public Set<HealthwiseRelatedContent> getRelatedContent() {
		return relatedContent;
	}

	public void setRelatedContent(Set<HealthwiseRelatedContent> relatedContent) {
		this.relatedContent = relatedContent;
	}

	public Set<String> getWorksConsulted() {
		return worksConsulted;
	}

	public void setWorksConsulted(Set<String> worksConsulted) {
		this.worksConsulted = worksConsulted;
	}

	@Override
	public String toString() {
		StringBuilder value = new StringBuilder();
		value.append(hwid + ", " + type + ", " + filename + ", " + title);
		value.append("\n" + content);

		return value.toString();
	}

	public String getSubtype() {
		return subtype;
	}

	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}
}

package com.optum.sync.healthwise.model;

public class HealthwiseMedia extends HealthwiseContent {

	private String filename;

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
}

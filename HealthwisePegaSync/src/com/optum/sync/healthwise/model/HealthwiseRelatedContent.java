package com.optum.sync.healthwise.model;

public class HealthwiseRelatedContent extends HealthwiseContent implements
		Comparable<HealthwiseRelatedContent> {

	private String type;
	private String rank;
	private String title;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Sort first by rank and then by title. If the hwid's are equal then so is
	 * the comparison.
	 * 
	 * @param other
	 * @return
	 */
	public int compareTo(HealthwiseRelatedContent other) {
		// compare hwids first
		if (hwid != null && other.hwid != null) {

			// if the hwid's are the same then they are equal so skip comparison
			int hwCompare = hwid.compareTo(other.hwid);
			if(hwCompare != 0) {
	
				// compare ranks
				if (rank != null && other.rank != null) {
					int rankComparison = rank.compareTo(other.rank);
					if (rankComparison == 0) {

						// compare titles if the ranks are equal
						if (title != null && other.title != null) {
							return title.compareTo(other.title);
						} else if (title == null && other.title != null) {
							return -1;
						} else if (title != null && other.title == null) {
							return 1;
						} else {
							// compare hwids if both the rank and title are equal
							return hwCompare;
						}
						
					} else {
						return rankComparison;
					}

				} else if (rank == null && other.rank != null) {
					return -1;
				} else if (rank != null && other.rank == null) {
					return 1;
				} else {

					// compare titles if the ranks are equal, both null
					if (title != null && other.title != null) {
						return title.compareTo(other.title);
					} else if (title == null && other.title != null) {
						return -1;
					} else if (title != null && other.title == null) {
						return 1;
					} else {
						// compare hwids if both the rank and title are equal
						return hwCompare;
					}
				}
			} else {
				// the hwid's are equal
				return 0;
			}

		// ignore rank and title if the hwid is null
		} else if (hwid == null && other.hwid != null) {
			return -1;
		} else if (hwid != null && other.hwid == null) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
}

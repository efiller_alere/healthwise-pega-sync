package com.optum.sync.healthwise.model;

import java.util.Locale;

public class HealthwiseContent  {

	protected String hwid;
	protected Locale locale;

	public HealthwiseContent() {
		super();
	}

	public String getHwid() {
		return hwid;
	}

	public void setHwid(String hwid) {
		this.hwid = hwid;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hwid == null) ? 0 : hwid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HealthwiseContent other = (HealthwiseContent) obj;
		if (hwid == null) {
			if (other.hwid != null)
				return false;
		} else if (!hwid.equals(other.hwid))
			return false;
		return true;
	}

}
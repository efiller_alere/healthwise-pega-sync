package com.optum.sync.healthwise.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SolrDocument {

	protected String id;
	protected String name;
	protected String title;
	protected String author;
	protected String type;
	protected String summary;
	protected String locale;
	protected String literacyLevel;
	protected Set<String> keywords = new HashSet<String>();
	protected Set<String> clients = new HashSet<String>();
	protected Set<String> categories = new HashSet<String>();
	protected Set<String> tags = new HashSet<String>();
	protected Boolean published;
	protected String publishedDate;
	protected String publishedYear;
	protected String path;
//	protected Map<QName, Serializable> properties;
	protected Date   modifiedDate;   
    protected String modifiedDateString;

	protected Set<String> text = new HashSet<String>();
	protected Set<String> text_es = new HashSet<String>();
	protected String content;

    
    public String getModifiedDateString() {
        return modifiedDateString;
    }
    public void setModifiedDateString(String modifiedDateString) {
        this.modifiedDateString = modifiedDateString;
    }

    
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}
	
	public String getLiteracyLevel() {
		return literacyLevel;
	}

	public void setLiteracyLevel(String literacyLevel) {
		this.literacyLevel = literacyLevel;
	}

	public Set<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(Set<String> keywords) {
		this.keywords = keywords;
	}
	
	public Set<String> getClients() {
		return clients;
	}

	public void setClients(Set<String> clients) {
		this.clients = clients;
	}

	public Set<String> getCategories() {
		return categories;
	}

	public void setCategories(Set<String> categories) {
		this.categories = categories;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public Boolean getPublished() {
		return published;
	}

	public void setPublished(Boolean published) {
		this.published = published;
	}

	public String getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getPublishedYear() {
		return publishedYear;
	}

	public void setPublishedYear(String publishedYear) {
		this.publishedYear = publishedYear;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
/*	
	public Map<QName, Serializable> getProperties() {
		return properties;
	}

	public void setProperties(Map<QName, Serializable> properties) {
		this.properties = properties;
	}
*/
	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Set<String> getText() {
		return text;
	}
	public void setText(Set<String> text) {
		this.text = text;
	}
	public Set<String> getText_es() {
		return text_es;
	}
	public void setText_es(Set<String> text_es) {
		this.text_es = text_es;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
package com.optum.sync.healthwise.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SolrDocumentNextGen extends SolrDocument {

	// NextGen fields
	private String healthwiseId;
	private String alereContentId;
	private String likes;
	private String imagePath;
	
	private String subType;
	private String displayOrder;
	private String startDate;
	private String endDate;
 	


	
	public SolrDocumentNextGen(SolrDocument baseDocument) {
		this.id = baseDocument.getId();
		this.name = baseDocument.getName();
		this.title = baseDocument.getTitle();
		this.author = baseDocument.getAuthor();
		this.type = baseDocument.getType();
		this.summary = baseDocument.getSummary();
		this.locale = baseDocument.getLocale();
		this.literacyLevel = baseDocument.getLiteracyLevel();
		this.keywords = (baseDocument.getKeywords() instanceof Set) ? baseDocument.getKeywords() : new HashSet<String>();
		this.clients = (baseDocument.getClients() instanceof Set) ? baseDocument.getClients() : new HashSet<String>();
		this.categories = (baseDocument.getCategories() instanceof Set) ? baseDocument.getCategories() : new HashSet<String>();
		this.tags = (baseDocument.getTags() instanceof Set) ? baseDocument.getTags() : new HashSet<String>();
		this.published = baseDocument.getPublished();
		this.publishedDate = baseDocument.getPublishedDate();
		this.publishedYear = baseDocument.getPublishedYear();
		this.path = baseDocument.getPath();
//		this.properties = (baseDocument.getProperties() instanceof Map) ? baseDocument.getProperties() : new HashMap<QName, Serializable>();
        this.modifiedDate       = baseDocument.getModifiedDate();
        this.modifiedDateString = baseDocument.getModifiedDateString();
        this.text = baseDocument.getText();
        this.text_es = baseDocument.getText_es();
        this.content = baseDocument.getContent();
	}

	public String getHealthwiseId() {
		return healthwiseId;
	}

	public void setHealthwiseId(String healthwiseId) {
		this.healthwiseId = healthwiseId;
	}

	public String getAlereContentId() {
		return alereContentId;
	}

	public void setAlereContentId(String alereContentId) {
		this.alereContentId = alereContentId;
	}

	public String getLikes() {
		return likes;
	}

	public void setLikes(String likes) {
		this.likes = likes;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}
	
	public String getSubType() {
		return subType;
	}

	public String getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(String displayOrder) {
		this.displayOrder = displayOrder;
	}

	
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

 

 
}

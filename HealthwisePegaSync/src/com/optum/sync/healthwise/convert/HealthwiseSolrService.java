package com.optum.sync.healthwise.convert;

import java.util.List;



import com.optum.sync.healthwise.category.ExcelCategoryMapper;
import com.optum.sync.healthwise.model.HealthwiseDocument;

/**
 * This class contains functionality related to Solr document indexing and
 * search features.
 * 
 * @author rholder
 * 
 */
public interface HealthwiseSolrService {
	
	public void setSolrHost(String host);
	public void setMapper(ExcelCategoryMapper mapper);
	
	/**
	 * Process the given list of documents, posting the deletion data to the
	 * Solr server in batch increments.
	 * 
	 * @param documents
	 * @param element
	 */
	public void processAdds(List<HealthwiseDocument> hwDocs);

}

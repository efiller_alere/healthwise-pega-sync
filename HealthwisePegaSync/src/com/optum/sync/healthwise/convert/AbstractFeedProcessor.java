package com.optum.sync.healthwise.convert;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * {@link FeedProcessor} implementations can inherit from this abstract class to
 * utilize common functionality.
 * 
 * @author rholder
 * 
 */
public abstract class AbstractFeedProcessor {

	private static final Log logger = LogFactory
			.getLog(AbstractFeedProcessor.class);

	/**
	 * Parse the date, returning null if it was unable to parse the given
	 * string.
	 * 
	 * @param rawDate
	 * @return
	 */
	protected Date parseDate(String rawDate, String[] parsePatterns) {
//		Date date = null;
//		try {
//			date = DateUtils.parseDate(rawDate, parsePatterns);
//		} catch (ParseException e) {
//			logger.error("Error parsing date: " + rawDate);
//			logger.debug(e.getMessage());
//		}
//
//		return date;
		return parseDate(rawDate, parsePatterns, Locale.getDefault());
	}

	/**
	 * <p>
	 * Parses a string representing a date by trying a variety of different
	 * parsers.
	 * </p>
	 * 
	 * <p>
	 * The parse will try each parse pattern in turn. A parse is only deemed
	 * successful if it parses the whole of the input string. If no parse
	 * patterns match, null is returned.
	 * </p>
	 * 
	 * @param str
	 *            the date to parse, not null
	 * @param parsePatterns
	 *            the date format patterns to use, see SimpleDateFormat, not
	 *            null
	 * @param locale
	 *            the locale whose date format symbols should be used
	 * @return the parsed date
	 * @throws IllegalArgumentException
	 *             if the date string or pattern array is null
	 */
	public static Date parseDate(String str, String[] parsePatterns,
			Locale locale) {
		if (str == null || parsePatterns == null) {
			throw new IllegalArgumentException(
					"Date and Patterns must not be null");
		}

		SimpleDateFormat parser = null;
		ParsePosition pos = new ParsePosition(0);
		for (int i = 0; i < parsePatterns.length; i++) {
			if (i == 0) {
				parser = new SimpleDateFormat(parsePatterns[0], locale);
			} else {
				parser.applyPattern(parsePatterns[i]);
			}
			pos.setIndex(0);
			Date date = parser.parse(str, pos);
			if (date != null && pos.getIndex() == str.length()) {
				return date;
			}
		}
		logger.error("Error parsing date: " + str);
		return null;
	}
}

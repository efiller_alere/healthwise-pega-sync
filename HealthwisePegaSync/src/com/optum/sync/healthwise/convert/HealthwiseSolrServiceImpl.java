/**
 * 
 */
package com.optum.sync.healthwise.convert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;



//import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.optum.sync.healthwise.category.ExcelCategoryMapper;
import com.optum.sync.healthwise.convert.HealthwiseDom4jDocumentXMLProcessor;
import com.optum.sync.healthwise.model.HealthwiseDocument;
import com.optum.sync.healthwise.model.SolrDocument;
import com.optum.sync.healthwise.model.SolrDocumentNextGen;

/**
 * An Healthwise SolrService implementation to send healthwise content to Solr
 * 
 * @author efiller
 *
 */
public class HealthwiseSolrServiceImpl implements HealthwiseSolrService {

//	private static final Log logger = LogFactory.getLog(HealthwiseSolrServiceImpl.class);
	
	protected static final String POST_ENCODING = "UTF-8";
	protected static String updateUrl = "/update?q=*%3A*&wt=xml";
	protected static final int SUMMARY_MAX = 200;
	protected static final int RECIPE_SUMMARY_MAX = 150;
	
	protected Properties globalProperties;
	
	// maximum number of instructions to send to the Solr instance at one time
	protected Integer batchUpdateMax;
	
	// Solr instance url for posting updates
	private String solrHost = ""; //"http://localhost:8082/solr/nextgencloud";
	
	

	private ExcelCategoryMapper mapper;

	private TransformerFactory transformerFactory;
	private Transformer transformer;
	private URIResolver uriResolver;


	
	public HealthwiseSolrServiceImpl() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Initialize the XSL transformation classes.
	 * 
	 * @throws TransformerException
	 */
	public void init() throws TransformerException {
		transformerFactory = TransformerFactory.newInstance();
		transformerFactory.setURIResolver(uriResolver);
		
		InputStream stylesheet = null;
 
		stylesheet = HealthwiseDom4jDocumentXMLProcessor.class.getResourceAsStream("/alfresco/module/ApolloAlfrescoExtensions/xsl/healthwise/99/Transformer_base.xsl");
		
		transformer = transformerFactory.newTransformer(new StreamSource(stylesheet));
		IOUtils.closeQuietly(stylesheet);
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.alere.alfresco.services.search.SolrService#optimize()
	 */
	public void optimize() {
		Document xmlDocument = DocumentHelper.createDocument();
		xmlDocument.addElement("optimize");

		postXML(xmlDocument);
	}
	
	public void processAdds(List<HealthwiseDocument> hwDocs) {
		if (this.solrHost == null) {
			throw new IllegalStateException("Solr Host not set");
		}
		
		Document xmlDocument = DocumentHelper.createDocument();
		Element root = xmlDocument.addElement("add");
		
		for (HealthwiseDocument hwDoc: hwDocs) {
			try {
				SolrDocument document = convertToSolrDocument(hwDoc);
				addDocument(root, document);
			}
			catch(Exception ex) {
//				logger.error("Error syncing " +  hwDoc.getHwid(), ex);
				ex.printStackTrace();
			}
		}
		postXML(xmlDocument);
	}
	
	/**
	 * Return a {@link SolrDocument} based on the given {@link NodeRef}. The
	 * document is not saved or persisted in any way using this method.
	 * 
	 * @param nodeRef
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected SolrDocument convertToSolrDocument(HealthwiseDocument hwDoc) {

		
		SolrDocumentNextGen document = new SolrDocumentNextGen(new SolrDocument());

		String localeString = "en_US";
		if (hwDoc.getLocale() != null) {
			localeString = hwDoc.getLocale().toString();
		}
		String docId = "hw-" + localeString + "-" + hwDoc.getHwid();

		document.setId(docId);

		// name
		document.setName(hwDoc.getHwid());

		// type
		document.setType(hwDoc.getSubtype());

		document.setPath("hw-" + localeString + "-/" + hwDoc.getType());

		document.setTitle(hwDoc.getTitle());

		document.setAuthor(hwDoc.getAuthor());


		String summaryValue = hwDoc.getDocumentAbstract();
		if (summaryValue == null) {
			summaryValue = hwDoc.getContent();
		}

		if (summaryValue != null) {

			int index = summaryValue.indexOf("<p>");
			if (index != -1) {
				summaryValue = summaryValue.substring(index + 3);
			}
			summaryValue = summaryValue.replaceAll("\\<.*?\\>", "");
			summaryValue = summaryValue.replaceAll("\t", "");
			summaryValue = summaryValue.replaceAll("\n", "");

		}
		
		if (summaryValue != null && summaryValue.length() > SUMMARY_MAX) {
			document.setSummary(summaryValue.substring(0,SUMMARY_MAX));
		}
		else {
			document.setSummary(summaryValue);
		}
		document.setLocale(localeString);
		document.setLiteracyLevel("1");

		document.setPublished(true);
		Set<String> keywords = hwDoc.getKeywords();
	
		document.getKeywords().addAll(keywords);
		
		// categories
		Set<String> categories = mapper.getMatchingSet(keywords,hwDoc.getCategories());

		document.getCategories().addAll(categories);
		
		// Solr last modified date
		document.setModifiedDate(new Date());
        document.setModifiedDateString("");

		String content = convertContentToText(hwDoc.getContent());
		document.setContent(content);
        
		document.setHealthwiseId(hwDoc.getHwid());

		document.setAuthor(hwDoc.getAuthor());
		
		// Add additional NextGen Properties

		int likes = 0; 
		document.setLikes(Integer.toString(likes));
		document.setSubType(hwDoc.getType());

		if (document.getLocale().equals("en_US")
				|| document.getLocale().equals("en_US_")) {
			// write to text field
			document.getText().addAll(document.getKeywords());
			document.getText().add(document.getTitle());
			if (document.getSummary() != null) {
				document.getText().add(document.getSummary());
			}
			
		}
		else if (document.getLocale().equals("es_US")
				|| document.getLocale().equals("es_US_")) {
			// write to text_es field
			document.getText_es().addAll(document.getKeywords());
			document.getText_es().add(document.getTitle());
			if (document.getSummary() != null) {
				document.getText().add(document.getSummary());
			}
		}
        
        
		return document;
	}
	

	
	/**
	 * Post the XML document data to the Solr server.
	 * 
	 * @param xmlDocument
	 */
	protected void postXML(Document xmlDocument) {

		StringWriter stringWriter = new StringWriter();

		try 
		{
			XMLWriter xmlWriter = new XMLWriter(stringWriter);

			xmlWriter.write(xmlDocument);
			xmlWriter.close();

			Reader reader = new StringReader(stringWriter.toString());
			postData(reader, stringWriter.toString().length());
			
		} 
		catch (IOException e)
		{
			java.lang.System.err.println("Error with Data sent to Solr:" + stringWriter.toString());
		}

	}
	
	/**
	 * Post the given data to the Solr server.
	 * Code adapted from test utility post.jar in the Solr distribution
	 * 
	 * @param data
	 */
	@SuppressWarnings("unused")
	protected void postData(Reader data, int length) {
		HttpURLConnection urlc = null;
		try {
			urlc = (HttpURLConnection) new URL(solrHost + updateUrl).openConnection();
			try {
				urlc.setRequestMethod("POST");
			} 
			catch (ProtocolException e) 
			{
				throw new RuntimeException("Error", e);
			}
			urlc.setDoOutput(true);
			urlc.setDoInput(true);
			urlc.setUseCaches(false);
			urlc.setAllowUserInteraction(false);
			urlc.setRequestProperty("Content-type", "text/xml; charset=" + POST_ENCODING);
			OutputStream out = urlc.getOutputStream();
		

			try {
				Writer writer = new OutputStreamWriter(out, POST_ENCODING);
				IOUtils.copy(data, writer);
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (out != null)
					out.close();
			}

			InputStream in = urlc.getInputStream();
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(in));
				String line;
				while ((line = reader.readLine()) != null) {
					//logger.debug(line);
				}
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (in != null)
					in.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
			
		} finally {
			if (urlc != null)
				urlc.disconnect();
		}
	}
	
	/**
	 * Return a date {@link String} compatible with Solr's expected date format.
	 * 
	 * @param date
	 * @return
	 */
	protected String formatDate(Date date) {
		
		String dateString = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(date);
		if (!dateString.endsWith("Z")) {
			dateString += "Z";
		}
		return dateString;
	}
	
	/**
	 * Return a date {@link Date} compatible with Solr's expected date format.
	 * 
	 * @param dateString
	 * @return
	 */
	protected Date formatDate(String dateString) {

		if (dateString.endsWith("Z")) {
			dateString = StringUtils.removeEnd(dateString, "Z");
		}
		
		Date date = null;
		
		try
		{
			date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(dateString);
		}
		catch(Exception e)
		{
			// Quietly error out
		}
		
		if(date == null)
		{
			// If we cannot parse the date, then lets give it a very old date so it can be updated with a parseable date
			dateString ="2000-01-01T12:00:00.000-04:00";
			try {
				date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(dateString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return date;
	}
	

	/**
	 * Convert and add the given {@link SolrDocument} data to the specified XML
	 * element.
	 * 
	 * @param root
	 * @param document
	 */
	protected Element addDocument(Element root, SolrDocument baseDocument) {

		SolrDocumentNextGen document = (SolrDocumentNextGen) baseDocument;

		Element docElement = root.addElement("doc");

		docElement.addElement("field").addAttribute("name", "id").addText(
				document.getId());

		docElement.addElement("field").addAttribute("name", "name").addText(
				document.getName());

		docElement.addElement("field").addAttribute("name", "type").addText(
				document.getType());

		docElement.addElement("field").addAttribute("name", "path").addText(
				document.getPath());
		
		docElement.addElement("field").addAttribute("name", "locale").addText(
				document.getLocale());

		if (document.getTitle() != null) {
			docElement.addElement("field").addAttribute("name", "title")
					.addText(document.getTitle());
		}

		if (document.getAuthor() != null) {
			docElement.addElement("field").addAttribute("name", "author")
					.addText(document.getAuthor());
		}

		if (document.getPublished() != null) {
			docElement.addElement("field").addAttribute("name", "published")
					.addText(document.getPublished().toString());
		}

		if (document.getPublishedDate() != null) {
			docElement.addElement("field")
					.addAttribute("name", "publishedDate").addText(
							document.getPublishedDate());
		}

		if (document.getPublishedYear() != null) {
			docElement.addElement("field")
					.addAttribute("name", "publishedYear").addText(
							document.getPublishedYear());
		}

		if (document.getSummary() != null) {
			docElement.addElement("field").addAttribute("name", "summary")
					.addText(document.getSummary());
		}

		for (String keyword : document.getKeywords()) {
			if (keyword != null) {
				docElement.addElement("field").addAttribute("name", "keyword")
				.addText(keyword);
			}
		}
		
		for (String client : document.getClients()) {
			if (client != null) {
				docElement.addElement("field").addAttribute("name", "client")
				.addText(client);
			}
		}

		for (String tag : document.getTags()) {
			if (tag != null) {
				docElement.addElement("field").addAttribute("name", "tag").addText(
						tag);
			}
		}

		for (String category : document.getCategories()) {
			if (category != null) {
				docElement.addElement("field").addAttribute("name", "category")
				.addText(category);
			}
		}
		
		
		if (document.getModifiedDate() != null) {
			docElement.addElement("field").addAttribute("name", "modifiedDate")
					.addText(formatDate(document.getModifiedDate()));
		}
		
        if (document.getModifiedDateString() != null) {
            docElement.addElement("field")
                    .addAttribute("name", "modifiedDateString").addText(
                            document.getModifiedDateString());
        }
		

		// Add additional NextGen properties 
		if (document.getHealthwiseId() != null) {
			docElement.addElement("field").addAttribute("name", "healthwiseId")
					.addText(document.getHealthwiseId());
		}

		if (document.getAlereContentId() != null) {
			docElement.addElement("field").addAttribute("name", "alereContentId")
					.addText(document.getAlereContentId());
		}

		if (document.getLikes() != null) {
			docElement.addElement("field").addAttribute("name", "likes")
					.addText(document.getLikes());
		}

		if (document.getImagePath() != null) {
			docElement.addElement("field").addAttribute("name", "imagePath")
					.addText(document.getImagePath());
		}

		if (document.getSubType() != null) {
			docElement.addElement("field")
					.addAttribute("name", "subType").addText(
							document.getSubType());
		}

		if (document.getDisplayOrder() != null) {
			docElement.addElement("field")
					.addAttribute("name", "display_order").addText(
							document.getDisplayOrder());
		}

		if (document.getText() != null) {
			for (String text : document.getText()) {
				if (text != null) {
					docElement.addElement("field").addAttribute("name", "text")
					.addText(text);
				}
			}
		}
		
		if (document.getText_es() != null) {
			for (String text : document.getText_es()) {
				if (text != null) {
					docElement.addElement("field").addAttribute("name", "text_es")
					.addText(text);
				}
			}
		}
		 
		if (document.getContent() != null) {
			docElement.addElement("field")
			.addAttribute("name", "content").addText(
					document.getContent());
		}
		 
		
		
		if (document.getEndDate() != null) {
			docElement.addElement("field")
					.addAttribute("name", "endDate").addText(
							document.getEndDate());
		}
		
		if (document.getStartDate() != null) {
			docElement.addElement("field")
					.addAttribute("name", "startDate").addText(
							document.getStartDate());
		}
		

        
        
		return docElement;
	}
	
	/**
	 * Read in the given document stream and populate the document with its
	 * transformed content data.
	 * 
	 * @param document
	 * @param contentStream
	 * @throws TransformerException
	 */
	private void processDocumentContent(HealthwiseDocument document, InputStream contentStream) throws TransformerException {
//		logger.debug("Transforming : " + document.getHwid());
		
		// transform the given document
		StringWriter output = new StringWriter();
		transformer.transform(new StreamSource(contentStream), new StreamResult(output));
		if ( document.getLocale().getLanguage().equals("es")) {
			transformer.setParameter("lang", "/es");
		}
		else {
			transformer.setParameter("lang", "");
		}
		document.setContent(output.toString());
	}
	
	/**
	 * for now just return the string passed in.
	 * @param content
	 * @return
	 */
	private String convertContentToText(String content) {
        return content;

	}


	public void setGlobalProperties(Properties globalProperties) {
		this.globalProperties = globalProperties;
	}
	

	public void setBatchUpdateMax(Integer batchUpdateMax) {
		this.batchUpdateMax = batchUpdateMax;
	}

	public void setUpdateUrl(String updateUrl) {
		this.updateUrl = updateUrl;
	}

	public ExcelCategoryMapper getMapper() {
		return mapper;
	}

	public void setMapper(ExcelCategoryMapper mapper) {
		this.mapper = mapper;
	}

	public String getSolrHost() {
		return solrHost;
	}

	public void setSolrHost(String solrHost) {
		this.solrHost = solrHost;
	}

	
}

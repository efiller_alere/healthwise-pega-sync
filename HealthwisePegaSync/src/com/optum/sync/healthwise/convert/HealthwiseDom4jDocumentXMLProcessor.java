package com.optum.sync.healthwise.convert;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.ReaderInputStream;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.optum.sync.healthwise.convert.AbstractFeedProcessor;
import com.optum.sync.healthwise.model.HealthwiseDocument;
import com.optum.sync.healthwise.model.HealthwiseRelatedContent;

/**
 * This class is responsible for parsing the Healthwise raw zip content archive
 * and producing a list of POJO document objects that contain information
 * relevant to the current Alfresco content model.
 * 
 * @author rholder
 * 
 */
public class HealthwiseDom4jDocumentXMLProcessor extends AbstractFeedProcessor  {

	private static final Locale EN_US = new Locale("en", "US");
	private static final Locale ES_US = new Locale("es", "US");

	private static final String SYMPTOM_CHECKER_TITLE_ENGLISH = "Symptom Checker";
	private static final String SYMPTOM_CHECKER_TITLE_SPANISH = "Verificador de síntomas";

	private static final String DELETED_WRONG_LOCALE = "deleted wrong locale";				

	
	// parse date formats, e.g. August 29, 1997 or 29 Augusto, 1997
	private static final String[] DATEFORMATS = { "MMM d, yyyy", "d MMM, yyyy" };

	private static final String ARTICLE_URI = "${ARTICLE_URI}";
	private static final String IMAGE_SERVER_URI = "${IMG_SERVER_URI}";
	private static final String IMG_URI = "${IMG_URI}";
	private static final String CONTENT_URI = "${CONTENT_URI}";
	private static final String HW_MEDIA_FOLDER = IMAGE_SERVER_URI + "alere/healthwise/99/media/";
	private static final String HEALTHWISE_PROTOCOL = "healthwise://";

	private static final String ARTICLE_PREFIX = ARTICLE_URI + HEALTHWISE_PROTOCOL;
	private static final String IMG_PREFIX = IMG_URI + HEALTHWISE_PROTOCOL;
	private static final String CONTENT_PREFIX = CONTENT_URI + HEALTHWISE_PROTOCOL;

	private TransformerFactory transformerFactory;
	private Transformer transformer;
	private URIResolver uriResolver;
	private boolean onlyProcessUpdates = false;

	private String transformURL;
	
	
	/**
	 * Initialize the XSL transformation classes.
	 * 
	 * @throws TransformerException
	 */
	public void init() throws TransformerException {
		transformerFactory = TransformerFactory.newInstance();
		transformerFactory.setURIResolver(uriResolver);
		
		InputStream stylesheet = null;
 
		stylesheet = HealthwiseDom4jDocumentXMLProcessor.class.getResourceAsStream("/Transform_for_sync.xsl");
			
				//"/stylesheet/Transformer_base.xsl");
		if (stylesheet == null ) {
			throw new IllegalStateException("Stylesheet not set");
		}
		
		transformer = transformerFactory.newTransformer(new StreamSource(stylesheet));
		IOUtils.closeQuietly(stylesheet);
	}
	
	/**
	 * Return the content of the stylesheet file for the transform
	 * 
	 * @return
	 */
	protected InputStream getXSTContent() {
		
        try {
        	

        	HttpClient client = new HttpClient();

        	HttpMethod method = new GetMethod(transformURL);
        	int statusCode = client.executeMethod(method);

        	return  method.getResponseBodyAsStream();
        }
        catch (Exception ex) {
        	ex.printStackTrace();
        }
		
		return null;
	}

	/**
	 * Read in the given document stream and populate the document with its
	 * metadata.
	 * 
	 * @param document
	 * @param documentStream
	 * @throws DocumentException
	 */
	public void processDocumentMetadata(HealthwiseDocument document, InputStream documentStream) throws DocumentException {
		
		SAXReader reader = new SAXReader();
		Document xmlDocument = reader.read(documentStream);

		Element root = xmlDocument.getRootElement();
		document.setRank(root.attributeValue("rank"));
		document.setWrittenByHealthwise(Boolean.valueOf(root.attributeValue("hwContent")));
		if (root.attributeValue("lang") != null) {
			document.setLocale(parseLocale(root.attributeValue("lang")));
		}
		if (root.attributeValue("subtype") != null) {
			document.setSubtype(root.attributeValue("subtype"));
		}
		else if (root.attributeValue("type") != null
				&& root.attributeValue("type").equals("CareSupport")) {
			document.setSubtype("article_hw_csp");
		}
		else {
			document.setSubtype("article");
			
		}

		
		processNavstack(document, root);
		
		processMetadata(document, root);
		
		processSections(document, root);
		
		/*
		if(!isMobile() && !isCsp()) {
			processContent(document, root);
		}
		*/
	}




	/**
	 * Add documents from the given InputStream to the list, expecting the
	 * stream to contain a manifest of all of the documents contained in the
	 * archive.
	 * 
	 * @param documents
	 * @param techdocs
	 * @throws DocumentException
	 */
	@SuppressWarnings("unchecked")
	private void processTechdocs(List<HealthwiseDocument> documents, InputStream techdocs) throws DocumentException {
		
		SAXReader reader = new SAXReader();
		Document xmlDocument = reader.read(techdocs);

		List<Element> statuses = xmlDocument.getRootElement().elements();
		for (Element status : statuses) {
			HealthwiseDocument document = new HealthwiseDocument();
			document.setFilename(status.attributeValue("filename"));
			document.setHwid(status.attributeValue("hwid-content"));
			document.setType(status.attributeValue("type"));
			document.setStatus(status.attributeValue("status"));
			document.setTitle(status.element("title").getData().toString());

			
			String docStatus = document.getStatus();
			if(onlyProcessUpdates) {
				// only add in deleted and updated docs when processing updates
				if (docStatus.equalsIgnoreCase("deleted")
						|| docStatus.equalsIgnoreCase("updated")
						|| docStatus.equalsIgnoreCase("unchanged")
						|| docStatus.equalsIgnoreCase("added")) {
					documents.add(document);
				}
			} else {
				// only add in non-deleted docs for a full import
				if (!docStatus.equalsIgnoreCase("deleted")) {
					documents.add(document);
				}
			}
		}
	}
	
	
	
	/**
	 * Read in the given document stream and populate the document with its
	 * metadata.
	 * 
	 * @param document
	 * @param documentStream
	 * @throws DocumentException
	 */
/*	
	private void processDocumentMetadata(HealthwiseDocument document, InputStream documentStream) throws DocumentException {
		
		SAXReader reader = new SAXReader();
		Document xmlDocument = reader.read(documentStream);

		Element root = xmlDocument.getRootElement();
		document.setRank(root.attributeValue("rank"));
		document.setWrittenByHealthwise(Boolean.valueOf(root.attributeValue("hwContent")));
		if (root.attributeValue("lang") != null) {
			document.setLocale(parseLocale(root.attributeValue("lang")));
		}

		if(!isMobile() && !isCsp())
			processNavstack(document, root);
		
		processMetadata(document, root);
		
		if(!isMobile())
			processSections(document, root);
*/		
		/*
		if(!isMobile() && !isCsp()) {
			processContent(document, root);
		}
		*/
//	}

	/**
	 * Read in the given document stream and populate the document with its
	 * transformed content data.
	 * 
	 * @param document
	 * @param contentStream
	 * @throws DocumentException 
	 * @throws TransformerException
	 */
	public void processDocumentContent(HealthwiseDocument document, InputStream contentStream) throws TransformerException  {
		
		//logger.debug("CSP : " + isCsp());


		
		// transform the given document
		StringWriter output = new StringWriter();
		transformer.transform(new StreamSource(contentStream), new StreamResult(output));
		if ( document.getLocale().getLanguage().equals("es")) {
			transformer.setParameter("lang", "/es");
		}
		else {
			transformer.setParameter("lang", "");
		}
		document.setContent(output.toString());
		
		
	}

	/**
	 * Add navstack entries found in the given root element to the document.
	 * 
	 * @param document
	 * @param root
	 */
	@SuppressWarnings("unchecked")
	private void processNavstack(HealthwiseDocument document, Element root) {
	
		Element prebuiltElement = root.element("doc.prebuilt");
		if (prebuiltElement != null) {
			Element navstackElement = prebuiltElement.element("prebuilt.navstack");
			if (navstackElement != null) {
				Element navstackEntriesElement = navstackElement.element("navstack.entries");
				navstackEntriesElement.setName("div");
				navstackEntriesElement.addAttribute("id", "hw-menu");
				List<Element> entriesElements = navstackEntriesElement.elements();
				for (Element entry : entriesElements) {
	
					// avoid processing the same link element more than once
					if (!entry.getName().equals("a")) {
						String section = entry.attributeValue("section-href");
	
						entry.setName("a");
						entry.addAttribute("href", "#" + section);
					}
	
					// rewrite link from the embedded images
					List<Element> imgNodes = entry.selectNodes("//img");
					for (Element imgElement : imgNodes) {
						rewriteImg(imgElement);
					}
				}
	
				document.setNavContent(StringEscapeUtils.unescapeXml(navstackEntriesElement.asXML()).trim());
			}
		}
	}

	/**
	 * Add metadata found in the given root element to the document.
	 * 
	 * @param document
	 * @param root
	 */
	@SuppressWarnings("unchecked")
	private void processMetadata(HealthwiseDocument document, Element root) {
		
		Element metadataElement = root.element("doc.meta-data");
		if (metadataElement == null) {
			return;
			
		}
		Element metadataSimpleCollection = metadataElement.element("meta-data.simple-collection");
		Element metadataTitle = metadataElement.element("meta-data.title");
			
		//Changes introduced in new CSP version:
			
		Element metadataTitles = metadataElement.element("meta-data.titles");
		List<Element> metadataTitleEntries = metadataTitles.elements();
			
		if(metadataTitleEntries.size() == 1) { 
				
			Element collectionElement = metadataTitleEntries.get(0);
			document.setTitle(collectionElement.getText());
				
		} else {
			for (Element collectionElement : metadataTitleEntries) {
				String audienceType = collectionElement.attributeValue("audience");
				String lang = collectionElement.attributeValue("lang");
				if (document.getType().equals("CareSupport") && lang == null) {
					document.setTitle(collectionElement.getText());
					break;
						
				}
				else if (lang == null && audienceType !=null && audienceType.equalsIgnoreCase("Consumer")) {
					document.setTitle(collectionElement.getText());
					break;
				}
			}
		}

		if(metadataTitle!=null && (document.getTitle() == null || document.getTitle().isEmpty())) {
			document.setTitle(metadataTitle.getText());

		}
		
		if (metadataSimpleCollection != null) {
			List<Element> metadataEntries = metadataSimpleCollection.elements();
			for (Element entry : metadataEntries) {
				String name = entry.attributeValue("name");
				if (entry.getText() != null && !entry.getText().isEmpty() ) {
					if (name.equalsIgnoreCase("ReleaseDate")) {
						document.setReleaseDate(parseDate(entry.getText(), DATEFORMATS, document.getLocale()));
					} else if (name.equalsIgnoreCase("Abstract")) {
						document.setDocumentAbstract(entry.getText());
					} else if (name.equalsIgnoreCase("author")) {
						document.setAuthor(entry.getText());
					} else if (name.equalsIgnoreCase("editor")) {
						document.setEditor(entry.getText());
					} else if (name.equalsIgnoreCase("associate-editor")) {
						document.setAssociateEditor(entry.getText());
					}
				}
			}
		}

		Element metadataCollection = metadataElement.element("meta-data.collections");
		if (metadataCollection != null) {
			List<Element> metadataEntries = metadataCollection.elements();
			for (Element collectionElement : metadataEntries) {
				String type = collectionElement.attributeValue("type");
				if (type.equalsIgnoreCase("Categories")) {
					processCategoryCollection(document, collectionElement);
				} else if (type.equalsIgnoreCase("Citations")) {
					processCollection(document.getCitations(), collectionElement);
				} else if (type.equalsIgnoreCase("RelatedTopics")) {
					processRelatedTopics(document, collectionElement);
				} else if (type.equalsIgnoreCase("WorksConsulted")) {
					processCollection(document.getWorksConsulted(), collectionElement);
				}
			}
		}
	}

	/**
	 * Process each section and its metadata from the current document root
	 * element.
	 * 
	 * @param document
	 * @param root
	 */
	@SuppressWarnings("unchecked")
	private void processSections(HealthwiseDocument document, Element root) {
		
		Element sectionsElement = root.element("doc.sections");
		if (sectionsElement != null) {
			// process each section found in the document
			List<Element> sectionsList = sectionsElement.elements();
			for (Element section : sectionsList) {
				Element sectionMetadata = section.element("section.meta-data");
				if (sectionMetadata != null) {
					Element sectionMetadataCollections = sectionMetadata.element("meta-data.collections");

					if (sectionMetadataCollections != null) {
						List<Element> sectionMetadataCollectionList = sectionMetadataCollections.elements();
						for (Element collectionElement : sectionMetadataCollectionList) {

							// deal with the different kinds of metadata found in
							// the data
							String type = collectionElement.attributeValue("type");
							if (type.equalsIgnoreCase("Keywords")) {
								processCollection(document.getKeywords(), collectionElement);
							}
						}
					}
				}
			}
			
		}
	}

	/**
	 * Add the item values found in the given collectionElement to the target.
	 * 
	 * @param target
	 * @param collectionElement
	 */
	@SuppressWarnings("unchecked")
	private void processCategoryCollection(HealthwiseDocument document, Element collectionElement) {
		
		List<Element> items = collectionElement.elements();
		for (Element item : items) {
			List<Element> values = item.elements("item.value");
// if more than one.  get one with lang atribute for spanish.  get one without lang for english
			if (values.size()  > 1) {
				for (Element value : values) {
					if (document.getLocale().equals(EN_US) && value.attribute("lang") == null) {
						String keyword = value.getText();
						document.getCategories().add(keyword.trim());
					}
					else if (value.attribute("lang") != null) {
						String keyword = value.getText();
						document.getCategories().add(keyword.trim());
					}
				}
			}
			else {
				String keyword = values.get(0).getText();
				document.getCategories().add(keyword.trim());
				
			}
			
		}
	}


	/**
	 * Add the item values found in the given collectionElement to the target.
	 * 
	 * @param target
	 * @param collectionElement
	 */
	@SuppressWarnings("unchecked")
	private void processCollection(Collection<String> target, Element collectionElement) {
		
		List<Element> items = collectionElement.elements();
		for (Element item : items) {
			String keyword = item.elementText("item.value");
			target.add(keyword.trim());
		}
	}

	/**
	 * Add the related topics found in the given collectionElement to the
	 * document.
	 * 
	 * @param document
	 * @param collectionElement
	 */
	@SuppressWarnings("unchecked")
	private void processRelatedTopics(HealthwiseDocument document, Element collectionElement) {
		
			
		List<Element> items = collectionElement.elements();
		for (Element item : items) {
			HealthwiseRelatedContent relatedTopic = new HealthwiseRelatedContent();
			relatedTopic.setHwid(item.attributeValue("hwid-meta-data"));
			relatedTopic.setType(item.attributeValue("document-type"));
			relatedTopic.setRank(item.attributeValue("rank"));
			relatedTopic.setTitle(item.getText());

			if(!document.getHwid().equals(relatedTopic.getHwid())) {
				document.getRelatedTopics().add(relatedTopic);
			}
		}
	}

	/**
	 * Process the content stored in the XML, generating the document's content
	 * property and filling in any other related content.
	 * 
	 * @param document
	 * @param root
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	private void processContent(HealthwiseDocument document, Element root) {
		
		Element sectionsElement = root.element("doc.sections");

		// process each section found in the document, generating a final
		// processed piece of content
		List<Element> sectionsList = sectionsElement.elements();
		StringBuilder content = new StringBuilder();
		for (Element section : sectionsList) {

			// section title
			Element sectionMetadata = section.element("section.meta-data");
			if (sectionMetadata != null) {
				Element sectionTitle = sectionMetadata.element("meta-data.title");
				if (sectionTitle != null) {
					String title = sectionTitle.getText();
					content.append("<h2 id="
							+ section.attributeValue("hwid-content") + ">"
							+ title + "</h2>");
				}
			}

			// section content
			Element sectionContent = section.element("section.std");
			
			if (sectionContent != null) {

				Set<HealthwiseRelatedContent> relatedContent = document.getRelatedContent();

				// add related content from the links inside the sections and
				// also rewrite the link
				List<Element> contentLinkNodes = sectionContent.selectNodes("//link.content");
				for (Element linkElement : contentLinkNodes) {
					HealthwiseRelatedContent linkContent = new HealthwiseRelatedContent();
					linkContent.setHwid(linkElement.attributeValue("document-href"));
					linkContent.setType(linkElement.attributeValue("document-type"));
					linkContent.setRank(linkElement.attributeValue("rank"));
					
					filterId(linkContent);
					if(!document.getHwid().equals(linkContent.getHwid())) {
						relatedContent.add(linkContent);
					}

					rewriteContentLink(linkElement);
				}

				// add related external content from the links inside the
				// sections and also rewrite the link
				List<Element> externalLinkNodes = sectionContent.selectNodes("//link.external");
				for (Element linkElement : externalLinkNodes) {
					HealthwiseRelatedContent linkContent = new HealthwiseRelatedContent();
					linkContent.setHwid(linkElement.attributeValue("href"));
					linkContent.setType(linkElement.attributeValue("type"));
					linkContent.setRank(linkElement.attributeValue("rank"));

					filterId(linkContent);
					if(!document.getHwid().equals(linkContent.getHwid())) {
						relatedContent.add(linkContent);
					}

					rewriteExternalLink(linkElement);
				}

				// add related content from the embedded swf's
				List<Element> flashNodes = sectionContent.selectNodes("//object/param[@name='movie']");
				for (Element flashElement : flashNodes) {
					HealthwiseRelatedContent flashContent = new HealthwiseRelatedContent();
					flashContent.setHwid(flashElement.attributeValue("value"));

					filterId(flashContent);
					if(!document.getHwid().equals(flashContent.getHwid())) {
						relatedContent.add(flashContent);
					}
				}

				// add related content from the embedded images
				List<Element> imgNodes = sectionContent.selectNodes("//img");
				for (Element imgElement : imgNodes) {
					HealthwiseRelatedContent imgContent = new HealthwiseRelatedContent();
					imgContent.setHwid(imgElement.attributeValue("src"));

					filterId(imgContent);
					if(!document.getHwid().equals(imgContent.getHwid())) {
						relatedContent.add(imgContent);
					}

					rewriteImg(imgElement);
				}

				// append all of the section content
				List<Element> sectionContentElements = sectionContent.elements();
				for (Element contentElement : sectionContentElements) {
					content.append(contentElement.asXML());
				}
			}
		}

		// keep the processed content
		document.setContent(content.toString());
	}

	/**
	 * Rewrite the img element to include a variable placeholder.
	 * 
	 * @param imgElement
	 */
	private void rewriteImg(Element imgElement) {
		String src = imgElement.attributeValue("src");

		// avoid processing the same image element more than once
		if (!src.startsWith(HW_MEDIA_FOLDER)) {
			String folder = imgElement.attributeValue("type");
			if(!StringUtils.isEmpty(folder))
				src = folder + "/" + src;
			
			imgElement.addAttribute("src", HW_MEDIA_FOLDER + src);
		}
	}

	/**
	 * Rewrite the content link element to include a variable placeholder and
	 * rename the element to correspond with the standard HTML syntax for links.
	 * 
	 * @param linkElement
	 */
	private void rewriteContentLink(Element linkElement) {
		String ref = linkElement.attributeValue("document-href");
		String section = linkElement.attributeValue("section-href");

		// avoid processing the same link element more than once
		if (!linkElement.getName().equals("a")) {
			linkElement.setName("a");
			linkElement.addAttribute("href", ARTICLE_PREFIX + ref + "#" + section);
		}
	}

	/**
	 * Rewrite the external link element to include a variable placeholder and
	 * rename the element to correspond with the standard HTML syntax for links.
	 * 
	 * @param linkElement
	 */
	private void rewriteExternalLink(Element linkElement) {
		String ref = linkElement.attributeValue("href");

		// avoid processing the same link element more than once
		if (!linkElement.getName().equals("a")) {
			linkElement.setName("a");
			linkElement.addAttribute("href", CONTENT_PREFIX + ref);
		}
	}

	/**
	 * Return a {@link Locale} from the given {@link String} passed in.
	 * 
	 * @param localeString
	 * @return
	 */
	private Locale parseLocale(String localeString) {
		if (localeString.equalsIgnoreCase("es-us")) {
			return ES_US;
		} else {
			return EN_US;
		}
	}

	/**
	 * Convert any pre-processed hwid's back to their original form. This is
	 * necessary when the same piece of content gets processed more than once,
	 * rewriting the hwid in the original DOM node. Related articles (all links
	 * that point to other articles) are a subset of related content (all
	 * content that is linked up by the document).
	 * 
	 * @param relatedContent
	 */
	private void filterId(HealthwiseRelatedContent relatedContent) {
		relatedContent.setHwid(relatedContent.getHwid()
				.replace(ARTICLE_PREFIX, "")
				.replace(IMG_PREFIX, "")
				.replace(CONTENT_PREFIX, ""));
	}

	public void setUriResolver(URIResolver uriResolver) {
		this.uriResolver = uriResolver;
	}

	public void setOnlyProcessUpdates(boolean onlyProcessUpdates) {
		this.onlyProcessUpdates = onlyProcessUpdates;
	}

	public void setTransformURL(String transformURL) {
		this.transformURL = transformURL;
	}

	/*
	public static void main(String[] args) {
		 
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		try { 
			
			System.out.println("transforming..");
			InputStream stylesheet = 
				new java.io.FileInputStream("c:/csp/h2/base.xsl");
		
			Transformer transformer = transformerFactory.newTransformer(new StreamSource(stylesheet));
			transformer.transform(new StreamSource(new java.io.FileInputStream("c:/csp/h2/fevr4.xml")), new StreamResult(System.out) );
			
			IOUtils.closeQuietly(stylesheet);
			
		} catch (Exception e) {
			System.out.println("error: "+e);
			e.printStackTrace(System.out);
		}
		 
	}
	
	public void testTransform() {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		try { 
			InputStream stylesheet = 
				new java.io.FileInputStream("c:/csp/Transformer_base.xsl");
		
			Transformer transformer = transformerFactory.newTransformer(new StreamSource(stylesheet));
			transformer.transform(new StreamSource(new java.io.FileInputStream("c:/csp/abk4194.xml")), new StreamResult(System.out) );
			
			IOUtils.closeQuietly(stylesheet);
			
		} catch (Exception e) {
			System.out.println("error: "+e);
			e.printStackTrace(System.out);
		}
	}
 	*/
	
}

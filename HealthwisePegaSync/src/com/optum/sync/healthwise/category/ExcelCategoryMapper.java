/**
 * 
 */
package com.optum.sync.healthwise.category;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
/*
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
*/
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

//import com.alere.alfresco.tools.AlereSpaceName;

/**
 * @author EFiller
 *
 */
public class ExcelCategoryMapper {

	private static final String XLS_NAME = "Vendor_Category_Mapping_Rules.xls";

	private Map<String,String> catMap = new HashMap<String, String>();

	private String categoryMappingURL;
	
	
	/**
	 * Take Healthwise keyword and category sets and return matching categories.
	 * @param keywordSet
	 * @param categorySet
	 * @return
	 */
	public Set<String> getMatchingSet(Set<String> keywordSet, Set<String> categorySet) {
		if (catMap.isEmpty()) {
			init();
		}
		Set returnSet = new HashSet<String>();
		for (String key:keywordSet) {
			if (catMap.containsKey(key) ) {
				returnSet.add(catMap.get(key));
			}
		}
		for (String key:categorySet) {
			if (catMap.containsKey(key) ) {
				returnSet.add(catMap.get(key));
			}
		}

		return returnSet;
		
	}

	private void init() {
		try {
			InputStream file = getXLSContent();
			
			//Get the workbook instance for XLS file 
			HSSFWorkbook workbook = new HSSFWorkbook(file);

			//Get first sheet from the workbook
			HSSFSheet sheet = workbook.getSheet("Healthwise");

			int rowNumber = 10;
			boolean moreToDo = true;
			while(moreToDo) {			
				Row row = sheet.getRow(rowNumber);

				Cell healthwiseColumn 	=row.getCell(1);
				String healthwiseValue = healthwiseColumn.getStringCellValue();
				if (healthwiseValue != null && !healthwiseValue.isEmpty()) {
					Cell alfrescoColumn 	=row.getCell(2);
					String alfrescoValue = alfrescoColumn.getStringCellValue();
					if (alfrescoValue != null && !alfrescoValue.isEmpty() ) {
						alfrescoValue = "/" + alfrescoValue.replace(" > ", "/"); 
					}
					catMap.put(healthwiseValue, alfrescoValue);
				}
				else {
					moreToDo = false;
				}
				
				++rowNumber;

				if (rowNumber > 100) moreToDo = false;

			}

			
		}
		catch (Exception ex) {
			throw new RuntimeException("Error creating category  map", ex);
		}
	}
	/**
	 * Return the content of the XLS file where the category tag mapping rules
	 * are stored.
	 * 
	 * @return
	 */
	protected InputStream getXLSContent() {
		
        try {
        	

        	HttpClient client = new HttpClient();

        	HttpMethod method = new GetMethod(categoryMappingURL);
        	int statusCode = client.executeMethod(method);

        	return  method.getResponseBodyAsStream();
        }
        catch (Exception ex) {
        	ex.printStackTrace();
        }
		
		return null;
	}



	public void setCategoryMappingURL(String categoryMappingURL) {
		this.categoryMappingURL = categoryMappingURL;
	}
	
}
